let tabs = document.querySelectorAll('.tabs-title');

tabs.forEach(function(tab, index){
    tab.addEventListener('click', function(){
        let currentTabData = document.querySelector(`.tab-content[data-tab-content = "${tab.dataset.tabMark }"]`);
        document.querySelector('.tab-content.open').classList.remove('open');
        document.querySelector('.tabs-title.active').classList.remove('active');
        currentTabData.classList.add('open');
        tab.classList.add('active');
    });
})
